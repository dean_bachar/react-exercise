/* eslint-disable react/jsx-filename-extension */
import React from "react";

import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import TablesPage from "./pages/tables_page";
import Navigator from "./componenets/navigator";
import { GlobalTagsProvider } from "./contexts/tags_contexts";

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(4, 0, 6),
  },
}));

function App() {
  const classes = useStyles();
  return (
    <Router>
      <CssBaseline />
      <AppBar position="sticky">
        <Navigator />
      </AppBar>
      <div className={classes.heroContent}>
        <Container>
          <GlobalTagsProvider>
            <Switch>
              <Route path="/">
                <TablesPage />
              </Route>
            </Switch>
          </GlobalTagsProvider>
        </Container>
      </div>
    </Router>
  );
}

export default App;
