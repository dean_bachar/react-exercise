import React, { useState, useEffect } from "react";
import Graph from "react-graph-vis";
import "vis/dist/vis.css";
import "vis/dist/vis.js";
import { getNodes } from "../tools/tags_graph_tools";

const BasicTagsGraph = ({ tags, edges, options }) => {
  const [network, setNetwork] = useState(null);
  useEffect(() => {
    if (network && tags) {
      const nodes = getNodes(tags);
      network.setData({ nodes, edges });
    }
    return () => {};
  }, [network, tags, edges]);

  return (
    <Graph
      graph={{ nodes: [], edges: [] }}
      options={options}
      getNetwork={(network) => {
        setNetwork(network);
      }}
    />
  );
};

export default BasicTagsGraph;
