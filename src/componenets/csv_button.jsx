import React from "react";
import Button from "@material-ui/core/Button";
import Papa from "papaparse";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  input: {
    display: "none",
  },
}));

const handleNewCSVs = (paths, handleNewMat) => {
  for (const path of paths) {
    const fileData = new FileReader();
    fileData.onloadend = (e) => handleNewMat(Papa.parse(e.target.result).data);
    fileData.readAsText(path);
  }
};

const CSVButton = ({ handleNewMat }) => {
  const classes = useStyles();
  return (
    <>
      <input
        accept="*.csv"
        id="contained-button-file"
        type="file"
        className={classes.input}
        onChange={(event) => {
          handleNewCSVs(event.target.files, handleNewMat);
        }}
      />
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span">
          Upload CSV!
        </Button>
      </label>
    </>
  );
};

export default CSVButton;
