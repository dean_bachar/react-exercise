import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import ButtonsArea from "./buttons_area";
import useGlobalTagsContext from "../hooks/useTagsContext";
const GroupButtons = ({ groupIdToShow, setGroupIdToShow }) => {
  const { groupIds } = useGlobalTagsContext();
  return (
    <ButtonsArea
      buttons={[
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            setGroupIdToShow((groupIdToShow) => groupIdToShow + 1);
          }}
          disabled={groupIds.length <= groupIdToShow + 1}
        >
          Next
        </Button>,
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            setGroupIdToShow((groupIdToShow) => groupIdToShow - 1);
          }}
          disabled={groupIdToShow == 0}
        >
          Back
        </Button>,
      ]}
    />
  );
};

export default GroupButtons;
