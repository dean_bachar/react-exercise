import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import MetaModelGraph from "./meta_model_graph";

const MetaModelDialog = ({ isDialogOpen, setIsDialogOpen }) => {
  return (
    <Dialog fullWidth maxWidth="lg" open={isDialogOpen}>
      <DialogTitle>Meta Model</DialogTitle>
      <DialogContent>
        <MetaModelGraph />
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          onClick={() => setIsDialogOpen(false)}
          color="primary"
        >
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default MetaModelDialog;
