import React from "react";
import Button from "@material-ui/core/Button";

const MetaModelDialogButton = ({ setIsDialogOpen }) => (
  <Button
    variant="outlined"
    color="primary"
    onClick={() => setIsDialogOpen(true)}
  >
    Show meta model
  </Button>
);

export default MetaModelDialogButton;
