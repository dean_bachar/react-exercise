import React from "react";
import "vis/dist/vis.css";
import "vis/dist/vis.js";
import useGlobalTagsContext from "../hooks/useTagsContext";
import BasicTagsGraph from "./basic_tag_graph";

const MetaModelGraph = () => {
  const { tags, edges } = useGlobalTagsContext();

  const options = {
    height: "600px",

    edges: { arrows: { to: false } },
  };

  return <BasicTagsGraph tags={tags} edges={edges} options={options} />;
};

export default MetaModelGraph;
