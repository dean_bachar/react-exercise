import React from "react";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TagsManagementTable from "./tags_management_table";
import TagsManagementGraph from "./tags_management_graph";

const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(4, 0, 6),
  },
}));

const TagsManagmentArea = () => {
  const classes = useStyles();
  return (
    <div className={classes.content}>
      <Paper elevation={2}>
        <TagsManagementTable />
        <TagsManagementGraph />
      </Paper>
    </div>
  );
};
export default TagsManagmentArea;
