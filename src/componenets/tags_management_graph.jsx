import React, { useState, useMemo, useEffect } from "react";
import Graph from "react-graph-vis";
import "vis/dist/vis.css";
import "vis/dist/vis.js";
import useGlobalTagsActionsContext from "../hooks/useManageTags";
import { getSortedToAndFrom } from "../tools/tags_graph_tools";
import BasicTagsGraph from "./basic_tag_graph";

const TagsManagementGraph = () => {
  const { tags, edges, addManageEdge, removeManageEdge } =
    useGlobalTagsActionsContext();
  const options = {
    height: "300px",
    edges: { arrows: { to: false } },
    manipulation: {
      addEdge: function (edgeData, callback) {
        const [to, from] = getSortedToAndFrom(edgeData);
        const id = JSON.stringify({ to, from });
        to !== from &&
          edges.findIndex((edge) => edge.to === to && edge.from === from) ===
            -1 &&
          !addManageEdge({ id, to, from }) &&
          callback(edgeData);
      },
      addNode: false,
      deleteEdge: function (edgeData, callback) {
        let { to, from } = JSON.parse(edgeData.edges[0]);
        [to, from] = getSortedToAndFrom({ to, from });
        removeManageEdge({ to, from });
        callback(edgeData);
      },
      deleteNode: false,
    },
  };

  return <BasicTagsGraph tags={tags} edges={edges} options={options} />;
};

export default TagsManagementGraph;
