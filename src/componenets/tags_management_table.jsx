import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import TagsManagementTableHead from "./tags_management_table_head";
import { makeStyles } from "@material-ui/core/styles";
import useGlobalTagsActionsContext from "../hooks/useManageTags";

const useStyles = makeStyles({
  container: {
    maxHeight: 340,
  },
});

const TagsManagementTable = () => {
  const classes = useStyles();
  const { mat } = useGlobalTagsActionsContext();
  return (
    <TableContainer className={classes.container} component={Paper}>
      <Table stickyHeader aria-label="simple table">
        <TagsManagementTableHead headerRow={mat[0]} />
        <TableBody>
          {mat.slice(1).map(
            (row, index) =>
              row && (
                <TableRow key={row.toString() + index}>
                  {row.map((cell, index) => (
                    <TableCell
                      key={row.toString() + cell.toString() + index}
                      align="right"
                    >
                      {cell}
                    </TableCell>
                  ))}
                </TableRow>
              )
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
export default TagsManagementTable;
