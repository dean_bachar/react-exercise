import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import useGlobalTagsActionsContext from "../hooks/useManageTags";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 100,
  },
}));

const TagsManagementTableHead = ({ headerRow }) => {
  const classes = useStyles();

  const {
    tagsOptions,
    headerToTag,
    addManageHeaderTag,
    removeManageHeaderTag,
  } = useGlobalTagsActionsContext();
  const getMenuItems = (headerCell) =>
    ["", ...tagsOptions]
      .filter(
        (option) =>
          !Object.values(headerToTag).includes(option) ||
          headerToTag[headerCell] === option
      )
      .map((option) => (
        <MenuItem value={option} key={option}>
          {option ? option : "No Tag"}
        </MenuItem>
      ));
  return (
    <TableHead>
      <TableRow>
        {headerRow.map((headerCell) => (
          <TableCell key={headerCell} align="right">
            <FormControl className={classes.formControl}>
              <InputLabel id={headerCell.toString() + "-label"}>
                {headerCell}
              </InputLabel>
              <Select
                labelId={headerCell.toString() + "-label"}
                id={headerCell.toString() + "-select"}
                value={headerToTag[headerCell] || ""}
                onChange={(e) => {
                  e.target.value === ""
                    ? removeManageHeaderTag(headerCell)
                    : addManageHeaderTag(headerCell, e.target.value);
                }}
              >
                {getMenuItems(headerCell)}
              </Select>
            </FormControl>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TagsManagementTableHead;
