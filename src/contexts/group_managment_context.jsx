import React, { createContext } from "react";

export const GroupManagmentContext = createContext();

export const GroupManagmentProvider = ({ children, groupId }) => {
  return (
    <GroupManagmentContext.Provider value={{ groupId }}>
      {children}
    </GroupManagmentContext.Provider>
  );
};
