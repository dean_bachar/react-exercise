import React, { useState, createContext, useMemo, useEffect } from "react";
import { getDictOfDictsWithMoreDict } from "../tools/dict_tools";
import { getTagOptions } from "../services/tags_service";

export const GlobalTagsContext = createContext();
export const GlobalTagsActionsContext = createContext();

export const GlobalTagsProvider = ({ children }) => {
  const [matById, setMatById] = useState({});
  const [headerToTagById, setHeaderToTagById] = useState({});
  const [edgesById, setEdgesById] = useState({});
  const [tagsOptions, setTagsOptions] = useState([]);
  const tags = useMemo(
    () => [
      ...new Set(
        Object.values(headerToTagById)
          .map((headerToTag) => Object.values(headerToTag))
          .flat()
      ),
    ],
    [headerToTagById]
  );
  const edges = useMemo(
    () =>
      [
        ...new Set(
          Object.values(edgesById)
            .flat()
            .map((edge) => JSON.stringify(edge))
        ),
      ].map((str) => JSON.parse(str)),
    [edgesById]
  );
  const groupIds = useMemo(() => Object.keys(matById), [matById]);
  useEffect(() => {
    updateTagsOptions();
    return () => {};
  }, []);
  const updateTagsOptions = async () => {
    const options = await getTagOptions();
    setTagsOptions(options);
  };
  const createManageGroup = (groupId, mat) => {
    setMatById((matById) => getDictOfDictsWithMoreDict(matById, groupId, mat));
    setHeaderToTagById((headerToTagById) =>
      getDictOfDictsWithMoreDict(headerToTagById, groupId, {})
    );
    setEdgesById((edgesById) =>
      getDictOfDictsWithMoreDict(edgesById, groupId, [])
    );
  };
  const addManageHeaderTag = (groupId, header, tag) => {
    setHeaderToTagById((headerToTagById) => {
      let copy = { ...headerToTagById };
      copy[groupId][header] = tag;
      return copy;
    });
  };
  const removeManageHeaderTag = (groupId, header) => {
    setHeaderToTagById((headerToTagById) => {
      let copy = { ...headerToTagById };
      delete copy[groupId][header];
      return copy;
    });
  };

  const addManageEdge = (groupId, edge) => {
    setEdgesById((edgesById) => {
      let copy = { ...edgesById };
      copy[groupId].push(edge);
      copy[groupId] = [...new Set(copy[groupId])];
      return copy;
    });
  };
  const removeManageEdge = (groupId, edge) => {
    setEdgesById((edgesById) => {
      let copy = { ...edgesById };
      const { to, from } = edge;
      const indexToDelete = copy[groupId].findIndex(
        (edge) => edge.to === to && edge.from === from
      );
      copy[groupId].splice(indexToDelete, 1);
      return copy;
    });
  };

  const getManageMat = (groupId) => (matById[groupId] ? matById[groupId] : []);
  const getManageEdges = (groupId) =>
    edgesById[groupId] ? edgesById[groupId] : [];
  const getManageheaderToTag = (groupId) =>
    headerToTagById[groupId] ? headerToTagById[groupId] : {};

  return (
    <GlobalTagsContext.Provider value={{ tags, edges, groupIds }}>
      <GlobalTagsActionsContext.Provider
        value={{
          tagsOptions,
          createManageGroup,
          addManageHeaderTag,
          removeManageHeaderTag,
          addManageEdge,
          removeManageEdge,
          getManageMat,
          getManageEdges,
          getManageheaderToTag,
        }}
      >
        {children}
      </GlobalTagsActionsContext.Provider>
    </GlobalTagsContext.Provider>
  );
};
