import { useContext } from "react";
import { GlobalTagsActionsContext } from "../contexts/tags_contexts";

const useCreateManageGroup = () => {
  const { createManageGroup } = useContext(GlobalTagsActionsContext);
  return {
    createManageGroup,
  };
};
export default useCreateManageGroup;
