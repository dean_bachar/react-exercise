import { useContext, useMemo } from "react";
import { GlobalTagsActionsContext } from "../contexts/tags_contexts";
import { GroupManagmentContext } from "../contexts/group_managment_context";

const useManageTags = () => {
  const { groupId } = useContext(GroupManagmentContext);
  const {
    tagsOptions,
    addManageHeaderTag,
    removeManageHeaderTag,
    addManageEdge,
    removeManageEdge,
    getManageMat,
    getManageEdges,
    getManageheaderToTag,
  } = useContext(GlobalTagsActionsContext);
  const tags = useMemo(
    () => Object.values(getManageheaderToTag(groupId)),
    [getManageheaderToTag, groupId]
  );
  return {
    tagsOptions,
    addManageHeaderTag: (header, tag) =>
      addManageHeaderTag(groupId, header, tag),
    removeManageHeaderTag: (header) => removeManageHeaderTag(groupId, header),
    addManageEdge: (edge) => addManageEdge(groupId, edge),
    removeManageEdge: (edge) => removeManageEdge(groupId, edge),
    mat: getManageMat(groupId),
    tags,
    edges: getManageEdges(groupId),
    headerToTag: getManageheaderToTag(groupId),
  };
};
export default useManageTags;
