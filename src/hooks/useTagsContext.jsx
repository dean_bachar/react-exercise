import { useContext } from "react";
import { GlobalTagsContext } from "../contexts/tags_contexts";

const useGlobalTagsContext = () => useContext(GlobalTagsContext);
export default useGlobalTagsContext;
