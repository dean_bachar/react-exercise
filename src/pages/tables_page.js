import React, { useState } from "react";

import CSVButton from "../componenets/csv_button";
import TagsManagmentArea from "../componenets/tags_manage_area";
import MetaModelDialogButton from "../componenets/meta_model_dialog_button";
import MetaModelDialog from "../componenets/meta_model_dialog";
import ButtonsArea from "../componenets/buttons_area";
import useGlobalTagsContext from "../hooks/useTagsContext";
import useCreateManageGroup from "../hooks/useCreateManageGroup";
import { GroupManagmentProvider } from "../contexts/group_managment_context";
import GroupButtons from "../componenets/group_buttons";
const TablesPage = () => {
  const { groupIds } = useGlobalTagsContext();
  const { createManageGroup } = useCreateManageGroup();
  const [isMetaDialogOpen, setIsMetaDialogOpen] = useState(false);
  const [groupIdToShow, setGroupIdToShow] = useState(0);
  const managementButtons = [
    <CSVButton
      handleNewMat={(newMat) => createManageGroup(groupIds.length, newMat)}
    />,
    <MetaModelDialogButton setIsDialogOpen={setIsMetaDialogOpen} />,
  ];
  return (
    <>
      <ButtonsArea buttons={managementButtons} />
      {groupIds.length > 0 && (
        <GroupManagmentProvider groupId={groupIdToShow}>
          <TagsManagmentArea />
        </GroupManagmentProvider>
      )}

      <GroupButtons
        groupIdToShow={groupIdToShow}
        setGroupIdToShow={setGroupIdToShow}
      />
      <MetaModelDialog
        isDialogOpen={isMetaDialogOpen}
        setIsDialogOpen={setIsMetaDialogOpen}
      />
    </>
  );
};

export default TablesPage;
