import axios from "axios";

export async function getTagOptions() {
  const results = await axios.get("/tagOptions");
  return results.data.map((reault) => reault["tag"]);
}
