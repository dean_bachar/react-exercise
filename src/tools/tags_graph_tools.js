export const getNodes = (tags) => tags.map((tag) => ({ id: tag, label: tag }));
export const getSortedToAndFrom = (edgeData) => {
  let toAndFrom = [edgeData.to, edgeData.from];
  toAndFrom.sort();
  return toAndFrom;
};
